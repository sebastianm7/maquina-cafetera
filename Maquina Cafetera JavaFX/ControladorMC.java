import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javax.swing.JOptionPane;
import javafx.scene.control.Button;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import javafx.application.Application; 
import javafx.scene.Scene; 
import javafx.scene.layout.*; 
import javafx.event.ActionEvent; 
import javafx.event.EventHandler; 
import javafx.scene.control.*; 
import javafx.stage.Stage; 
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.ImageView;



public class ControladorMC {

    @FXML
    private Label lblGananciasNetas;

    @FXML
    private Label txtCantCafe;

    @FXML
    private BorderPane panel1;

    @FXML
    private Label lblCantAzucar;

    @FXML
    private Label txtCantVasos;

    @FXML
    private Label txtGananciasAntesIVA;

    @FXML
    private Label txtIVA;

    @FXML
    private Label lblTitulo;

    @FXML
    private GridPane panelDatos;
    
    @FXML
    private GridPane panelImagenes;
    
    @FXML
    private ImageView imagenMotilon;
    
    @FXML
    private ImageView imagenMaquina;
    
    @FXML
    private Button cmdRecargarCafe;

    @FXML
    private Label lblCantVasos;

    @FXML
    private Label txtPrecio;

    @FXML
    private Label lblGananciasAntesIVA;

    @FXML
    private Label txtTotalIngresos;

    @FXML
    private Label txtCantAzucar;

    @FXML
    private Button cmdTipoCafe;

    @FXML
    private Label lblIVA;

    @FXML
    private Button cmdAgregado;

    @FXML
    private Button cmdPrepararCafe;

    @FXML
    private Label lblCantCafe;

    @FXML
    private Button cmdRecargarAzucar;

    @FXML
    private Label lblPrecio;

    @FXML
    private Label txtPrecioBaseCafe;

    @FXML
    private Button cmdRecargarVasos;

    @FXML
    private GridPane panelBotones;

    @FXML
    private Button cmdFijarPrecioBase;

    @FXML
    private Label txtGananciasNetas;

    @FXML
    private Label lblTotalEgresos;

    @FXML
    private Label lblTotalIngresos;

    @FXML
    private Label txtTotalEgresos;

    @FXML
    private Button cmdRegistrarFactura;

    @FXML
    private Label lblPrecooBaseCafe;
    
    private ModeloMC mc;
    
    public ControladorMC(){
    mc = new ModeloMC();
    }
    
    @FXML
    void tipoCafe() {
    List<String> choices = new ArrayList<>();
    choices.add("1- Tinto Sencillo");
    choices.add("2- Tinto Normal");
    choices.add("3- Tinto Doble");
    
    ChoiceDialog<String> dialog = new ChoiceDialog<>("1- Tinto Sencillo", choices);
    dialog.setTitle("Tipo Café");
    dialog.setHeaderText("Elija el tipo de café");
    dialog.setContentText("Tipo Café:");
    
    Optional<String> result = dialog.showAndWait();
    
    if(result.get() == "1- Tinto Sencillo"){
    mc.setTipoCafe(1);
    }
    if(result.get() == "2- Tinto Normal"){
    mc.setTipoCafe(2);
    }
    if(result.get() == "3- Tinto Doble"){
    mc.setTipoCafe(3);
    }
    }

    @FXML
    void fijarPrecioBase() {
    TextInputDialog dialog = new TextInputDialog("");
    dialog.setTitle("Precio Base Café");
    dialog.setHeaderText("Precio Base Café");
    dialog.setContentText("Ingrese valor:");
    
    Optional<String> result = dialog.showAndWait();
    
    if(Integer.parseInt(result.get())<1000){
    Alert alert = new Alert(Alert.AlertType.ERROR, "Precio base no puede ser menor a 1000");
    alert.showAndWait();    
    }else{
    mc.setPrecioBaseCafe(Integer.parseInt(result.get()));
    
    txtPrecioBaseCafe.setText(Integer.toString(mc.getPrecioBaseCafe()));
    }
    }

    @FXML
    void recargarCafe() { 
    TextInputDialog cantidadCafe = new TextInputDialog();
    cantidadCafe.setTitle("Cantidad de Café");
    cantidadCafe.setHeaderText("Cantidad Café");
    cantidadCafe.setContentText("Ingrese cantidad de Café:");

    Optional<String> result = cantidadCafe.showAndWait();
    
    
    TextInputDialog costoCompraCafe = new TextInputDialog();
    costoCompraCafe.setTitle("Costo Compra Café");
    costoCompraCafe.setHeaderText("Costo Compra Café");
    costoCompraCafe.setContentText("Ingrese costo de compra de Café:");

    Optional<String> res = costoCompraCafe.showAndWait();
    
    
    if(mc.recargarCafe(Integer.parseInt(result.get()),Integer.parseInt(res.get()))){;
    
    txtCantCafe.setText(Integer.toString(mc.getCafe()));
    }else{
    Alert alert = new Alert(Alert.AlertType.ERROR, "No hay dinero suficiente para recargar café");
    alert.showAndWait();
    }
    }

    @FXML
    void prepararCafe() {
    if((mc.getTipoCafe() == 1 && mc.getCafe() < 55) || (mc.getTipoCafe() == 2 && mc.getCafe() < 110) || (mc.getTipoCafe() == 3 && mc.getCafe() < 165) || mc.getVasos() <= 0 ){
    Alert alert = new Alert(Alert.AlertType.ERROR, "No se puede preparar café, por favor revisar las cantidades necesarias para la preparación");
    alert.showAndWait(); 
    }else{                     
    mc.prepararCafe(mc.getTipoCafe(), mc.getAgregado());
    txtCantCafe.setText(Integer.toString(mc.getCafe()));
    txtCantAzucar.setText(Integer.toString(mc.getAzucar()));
    txtCantVasos.setText(Integer.toString(mc.getVasos()));
    Alert alert = new Alert(Alert.AlertType.INFORMATION, "Se preparo el café correctamente, por favor registre la factura");
    alert.showAndWait();
    }
    }

    @FXML
    void recargarVasos() {
    TextInputDialog cantidadVasos = new TextInputDialog();
    cantidadVasos.setTitle("Cantidad de Vasos");
    cantidadVasos.setHeaderText("Cantidad Vasos");
    cantidadVasos.setContentText("Ingrese cantidad de Vasos:");

    Optional<String> result = cantidadVasos.showAndWait();
    
    TextInputDialog costoCompraVasos = new TextInputDialog();
    costoCompraVasos.setTitle("Costo Compra Vasos");
    costoCompraVasos.setHeaderText("Costo Compra Vasos");
    costoCompraVasos.setContentText("Ingrese costo de compra de Vasos");

    Optional<String> res = costoCompraVasos.showAndWait();
    
    
    if(mc.recargarVasos(Integer.parseInt(result.get()), Integer.parseInt(res.get()))){;
    
    txtCantVasos.setText(Integer.toString(mc.getVasos()));
    }
    else{
    Alert alert = new Alert(Alert.AlertType.ERROR, "No hay dinero suficiente para recargar vasos");
    alert.showAndWait(); 
    }
    }

    @FXML
    void recargarAzucar() {
    TextInputDialog cantidadAzucar = new TextInputDialog();
    cantidadAzucar.setTitle("Cantidad de Azúcar");
    cantidadAzucar.setHeaderText("Cantidad Azúcar");
    cantidadAzucar.setContentText("Ingrese cantidad de Azúcar:");

    
    Optional<String> result = cantidadAzucar.showAndWait();
    
    TextInputDialog costoCompraAzucar = new TextInputDialog();
    costoCompraAzucar.setTitle("Costo Compra Azúcar");
    costoCompraAzucar.setHeaderText("Costo Compra Azúcar");
    costoCompraAzucar.setContentText("Ingrese costo de compra de Azúcar");

    Optional<String> res = costoCompraAzucar.showAndWait();
    
    if(mc.recargarAzucar(Integer.parseInt(result.get()), Integer.parseInt(res.get()))){;
    
    txtCantAzucar.setText(Integer.toString(mc.getAzucar()));
    }else{
    Alert alert = new Alert(Alert.AlertType.ERROR, "No hay dinero suficiente para recargar azúcar");
    alert.showAndWait(); 
    }
    }

    @FXML
    void registrarFactura() {
    txtTotalEgresos.setText(Integer.toString(mc.getEgresos()));
    mc.setIngresos(mc.getIngresos()+Integer.parseInt(txtPrecio.getText()));
    txtTotalIngresos.setText(Integer.toString(mc.getIngresos()));
    txtGananciasAntesIVA.setText(Integer.toString(mc.getGananciasBrutas()));
    txtIVA.setText(Integer.toString(mc.getImpuestos()));
    txtGananciasNetas.setText(Integer.toString(mc.getGananciasNetas()));
    }

    @FXML
    void agregado() {
    List<String> choices = new ArrayList<>();
    choices.add("1- Sin Azúcar");
    choices.add("2- Bajo en Azúcar");
    choices.add("3- Normal");
    
    ChoiceDialog<String> dialog = new ChoiceDialog<>("1- Sin Azúcar", choices);
    dialog.setTitle("Agregado");
    dialog.setHeaderText("Agregado");
    dialog.setContentText("Agregado:");
    
    Optional<String> result = dialog.showAndWait();
    
    if(result.get() == "1- Sin Azúcar"){
    mc.setAgregado(1);
    }
    if(result.get() == "2- Bajo en Azúcar"){
    mc.setAgregado(2);
    }
    if(result.get() == "3- Normal"){
    mc.setAgregado(3);
    }
    txtPrecio.setText(Integer.toString(mc.calcularPrecio(mc.getTipoCafe(), mc.getAgregado())));
    }

}