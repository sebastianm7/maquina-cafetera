
/**
 * Complete
 *
 * @author (Milton Jesús Vera Contreras - miltonjeussvc@ufps.edu.co)
 * @version 0.000000000000001 :) --> Math.sin(Math.PI-Double.MIN_VALUE)
 */
class ModeloMC {

    int cafe;
    int azucar;
    int vasos;
    int precioBaseCafe;
    int egresos;
    int ingresos;
    int gananciasBrutas;
    int impuestos;
    int gananciasNetas;
    int tipoCafe;
    int agregado;
    

    /*No requiere propiedades adicionales, pero es libre de usarlas*/

    ModeloMC() {
        //complete
        ingresos=0;
        gananciasBrutas=0;
        gananciasNetas=0;
    }

    ModeloMC(int cantidadInicialCafe, int cantidadInicialAzucar, int cantidadInicialVasos) {
        //complete
        this.cafe = cantidadInicialCafe;
        this.azucar = cantidadInicialAzucar;
        this.vasos = cantidadInicialVasos;
    }

    //complete metodos GET / SET
    public void setCafe(int cafe) {
        this.cafe = cafe;
    }
    
    public void setAgregado(int agregado){
    this.agregado = agregado;
    }
    
    public int getAgregado(){
    return this.agregado;
    }
    
    public void setTipoCafe(int tipoCafe){
    this.tipoCafe = tipoCafe;
    }
    
    public int getTipoCafe(){
    return this.tipoCafe;
    }
    
    public int getCafe() {
        return this.cafe;
    }

    public void setAzucar(int azucar) {
        this.azucar = azucar;
    }

    public int getAzucar() {
        return this.azucar;
    }

    public void setVasos(int vasos) {
        this.vasos = vasos;
    }

    public int getVasos() {
        return this.vasos;
    }

    public void setPrecioBaseCafe(int precioBaseCafe) {
        this.precioBaseCafe = precioBaseCafe;
    }

    public int getPrecioBaseCafe() {
        return this.precioBaseCafe;
    }

    public void setEgresos(int egresos) {
        this.egresos = egresos;
    }

    public int getEgresos() {
        return egresos;
    }

    public void setIngresos(int ingresos) {
        this.ingresos = ingresos;
    }

    public int getIngresos() {
        return ingresos;
    }

    public void setImpuestos(int impuestos) {
        this.impuestos = impuestos;
    }

    public int getImpuestos() {
        return this.getIngresos() * 16 / 100;
    }

    public void setGananciasBrutas(int gananciasBrutas) {
        this.gananciasBrutas = gananciasBrutas;
    }

    public int getGananciasBrutas() {
        return this.getIngresos() - this.getEgresos();
    }

    public void setGananciasNetas(int gananciasNetas) {
        this.gananciasNetas = gananciasNetas;
    }

    public int getGananciasNetas() {
        return this.getGananciasBrutas() - this.getImpuestos();
    }

    public int calcularPrecio(int tipoCafe, int cantidadAzucar) {
        //complete
        int precio = this.getPrecioBaseCafe() / 1000 * 55;
        switch (tipoCafe) {
            case 2:
                precio = precio * 2;
                break;
            case 3:
                precio = precio * 3;
            default:
                break;
        }
        switch (cantidadAzucar) {
            case 2:
                precio += precio * 5 / 100;
                break;
            case 3:
                precio += precio * 10 / 100;
            default:
                break;
        }
        precio += precio * 15 / 100;
        return precio;
    }

    public boolean recargarCafe(int cantidadCafe, int costoCompraCafe) {

        if (this.getGananciasNetas() >= costoCompraCafe || this.gananciasNetas >= costoCompraCafe) {
            this.cafe += cantidadCafe;
            this.registroFacturaEgreso(costoCompraCafe);
            return true;
        }
        return false;
    }

    public boolean recargarAzucar(int cantidadAzucar, int costoCompraAzucar) {
        //complete
        if (this.getGananciasNetas() >= costoCompraAzucar) {
            this.azucar += cantidadAzucar;
            this.registroFacturaEgreso(costoCompraAzucar);
            return true;
        }
        return false;
    }

    public boolean recargarVasos(int cantidadVasos, int costoCompraVasos) {
        //complete
        if (this.getGananciasNetas() >= costoCompraVasos) {
            this.vasos += cantidadVasos;
            this.registroFacturaEgreso(costoCompraVasos);
            return true;
        }
        return false;
    }

    public boolean prepararCafe(int tipoCafe, int cantidadAzucar) {
        //complete
        boolean valorRetorno = false;
        if (this.getVasos() > 0) {
            if (tipoCafe == 1 && this.getCafe() >= 55) {
                valorRetorno = this.restarCantidades(cantidadAzucar, 1);
            } else if (tipoCafe == 2 && this.getCafe() >= 55 * 2) {
                valorRetorno = this.restarCantidades(cantidadAzucar, 2);
            } else if (tipoCafe == 3 && this.getCafe() >= 55 * 3) {
                valorRetorno = this.restarCantidades(cantidadAzucar, 3);
            }
        }
        if (valorRetorno) {
            this.vasos--;
        }
        return valorRetorno;
    }

    public boolean restarCantidades(int cantidadAzucar, int tipo) {
        boolean valorRetorno = false;
        if (cantidadAzucar == 1) {
            valorRetorno = true;
        } else if (cantidadAzucar == 2 && this.getAzucar() >= 5) {
            this.azucar -= 5;
            valorRetorno = true;
        } else if (cantidadAzucar == 3 && this.getAzucar() >= 10) {
            this.azucar -= 10;
            valorRetorno = true;
        }
        if (valorRetorno) {
            this.cafe -= 55 * tipo;
        }
        return valorRetorno;
    }

    public void registrarFactura(int valorFactura) {
        this.ingresos += valorFactura;
    }
    
     public void registroFacturaEgreso(int valorFactura) {
        this.egresos += valorFactura;
    }
}
